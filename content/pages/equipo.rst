Equipo
######
:date: 2019-03-11 21:00
:slug: equipamiento


* Básico

  - PC de escritorio
  - Impresora

* Prototipos y experimentación

  - Arduino Uno
  - Stellaris LM4F120
  - Seeeduino Film
  - NodeMCU
  - Bluetooth Bee

* Maquinaria

  - Compresor
  - Taladro
  - Esmeriladora
  - Aerografo
  - Dremel 4000

* Servicios

  - Internet
  - Wifi
  - Control de acceso biometrico

* Voz IP

  - ATA Sipura SPA-2000
  - ATA Sipura SPA-2100
  - Tarjeta TDM Digium
  - Teléfono IP YWH200D-505

* Redes

  - Switch SMC TigerStack 6224M
  - Router Nexx WT3020
